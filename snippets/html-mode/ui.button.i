# -*- mode: snippet; require-final-newline: nil -*-
# name: ui.button.i
# key: ui.button.i
# binding: direct-keybinding
# --
<button class="ui icon button">
        <i class="$0 icon"></i>
        $1
</button>