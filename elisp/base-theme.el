(use-package spacemacs-theme
  :defer t
  :init
  (load-theme 'spacemacs-dark t))

(load "fira-settings.el")

(use-package dynamic-fonts
  :ensure t
  :pin melpa
  :config
  (setq dynamic-fonts-preferred-monospace-fonts '( "Mononoki" "Inconsolata" "Anonymice Powerline" "Consolas")
	dynamic-fonts-preferred-monospace-point-size 11)
  (dynamic-fonts-setup))

(provide 'base-theme)
