;; (package-initialize)
(add-to-list 'package-archives
	     '("melpa" . "http://melpa.org/packages/")
         '("elpy" . "http://jorgenschaefer.github.io/packages/"))

(when (not package-archive-contents)
  (package-refresh-contents))

(unless (package-installed-p 'use-package)
  (package-install 'use-package))
(require 'use-package)

(defconst private-dir  (expand-file-name "private" user-emacs-directory))
(defconst temp-dir (format "%s/cache" private-dir)
  "Hostname-based elisp temp directories")

;; Core settings
;; UTF-8 please
(set-charset-priority 'unicode)
(setq locale-coding-system   'utf-8)   ; pretty
(set-terminal-coding-system  'utf-8)   ; pretty
(set-keyboard-coding-system  'utf-8)   ; pretty
(set-selection-coding-system 'utf-8)   ; please
(prefer-coding-system        'utf-8)   ; with sugar on top
(setq default-process-coding-system '(utf-8-unix . utf-8-unix))

;; Emacs customizations
(setq confirm-kill-emacs                  'y-or-n-p
      confirm-nonexistent-file-or-buffer  t
      save-interprogram-paste-before-kill t
      mouse-yank-at-point                 t
      require-final-newline               t
      visible-bell                        nil
      ring-bell-function                  'ignore
      custom-file                         "~/.emacs.d/.custom.el"
      ;; http://ergoemacs.org/emacs/emacs_stop_cursor_enter_prompt.html
      minibuffer-prompt-properties
      '(read-only t point-entered minibuffer-avoid-prompt face minibuffer-prompt)

      ;; Disable non selected window highlight
      cursor-in-non-selected-windows     nil
      highlight-nonselected-windows      nil
      ;; PATH
      exec-path                          (append exec-path '("/usr/local/bin/"))
      indent-tabs-mode                   nil
      tab-always-indent                  'complete
      tab-stop-list                      (number-sequence 4 120 4)
      column-number-mode                 t
      delete-selection-mode              t
      inhibit-startup-message            t
      gc-cons-threshold                  80000000
      buffers-menu-max-size              30
      fringes-outside-margins            t
      frame-title-format                 "%b (%f)"
      use-package-always-ensure          t)

(setq-default tab-width 4)

;; Bookmarks
(setq
 ;; persistent bookmarks
 bookmark-save-flag                      t
 bookmark-default-file              (concat temp-dir "/bookmarks"))

;; Backups enabled, use nil to disable
(setq
 history-length                     10
 backup-inhibited                   t
 make-backup-files                  nil
 auto-save-default                  nil
 auto-save-list-file-name           (concat temp-dir "/autosave")
 create-lockfiles                   nil
 backup-directory-alist            `((".*" . ,(concat temp-dir "/backup/")))
 auto-save-file-name-transforms    `((".*" ,(concat temp-dir "/auto-save-list/") t)))

(unless (file-exists-p (concat temp-dir "/auto-save-list"))
		       (make-directory (concat temp-dir "/auto-save-list") :parents))

(fset 'yes-or-no-p 'y-or-n-p)
(global-auto-revert-mode t)


;; Disable toolbar & menubar
(menu-bar-mode -1)
(when (fboundp 'tool-bar-mode)
  (tool-bar-mode -1))
(when (  fboundp 'scroll-bar-mode)
  (scroll-bar-mode -1))

;; Server
(use-package server
  :ensure nil
  :config
  (setq server-name "server")
  (server-force-delete)
  (server-start))

;;spell checking
(use-package ispell
  :defer 15
  :config
  (setq ispell-program-name "c:/Users/hp/Documents/executable/hunspell/bin/hunspell.exe")
  (setq ispell-really-hunspell t)
  (setq
   ispell-local-dictionary-alist `(
								   ("en_US" "[[:alpha:]]" "[^[:alpha:]]" "[']" t ("-d" "en_US") nil utf-8)
								   ("en_GB" "[[:alpha:]]" "[^[:alpha:]]" "[']" t ("-d" "en_GB") nil utf-8)
								   ("spanish" "[[:alpha:]]" "[^[:alpha:]]" "[']" nil ("-d" "es_ES") t utf-8)
								   )))
;; Desktop
(desktop-save-mode)
;; Paren-mode
(show-paren-mode 1)
(setq show-paren-style 'mixed)
;; smartparents??
(electric-pair-mode)
;;inputs
(setq-default default-input-method 'spanish-postfix)

;; ibuffer
(global-set-key (kbd "C-x C-b") 'ibuffer)

;; Delete trailing whitespace before save
(add-hook 'before-save-hook 'delete-trailing-whitespace)
(load custom-file)

(provide 'base)
;;; base ends here
