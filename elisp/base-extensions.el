
(load "~/Documents/projects/lisp/hugo-blog-mode/hugo-blog-mode.el")
(setq hugo-blog-project "~/Documents/projects/books/yorodm-site")

(use-package company
  :bind (:map company-mode-map
	      ("C-c TAB" . company-complete))
  :config
  (add-hook 'after-init-hook 'global-company-mode))



(use-package lsp-mode
  :commands (lsp)
  :config
  (use-package company-lsp
    :config
    (add-to-list 'company-backends 'company-lsp)))

(use-package lsp-ui
  :hook (lsp-mode . lsp-ui-mode)
  :config
  (define-key lsp-ui-mode-map [remap xref-find-definitions] #'lsp-ui-peek-find-definitions)
  (define-key lsp-ui-mode-map [remap xref-find-references] #'lsp-ui-peek-find-references)
  (setq lsp-ui-sideline-enable nil
        lsp-ui-doc-enable t
        lsp-ui-flycheck-enable nil
        lsp-ui-imenu-enable t
		lsp-ui-sideline-ignore-duplicate t))

(use-package ediff
  :config
  (setq ediff-window-setup-function 'ediff-setup-windows-plain)
  (setq-default ediff-highlight-all-diffs 'nil)
  (setq ediff-diff-options "-w"))

(use-package helpful
  :bind
  ("C-h f" . helpful-callable)
  ("C-h v" . helpful-variable)
  ("C-c C-d" . helpful-at-point)
  ("C-h F" . helpful-function)
  ("C-h C" . helpful-command))

(use-package exec-path-from-shell
  :config
  ;; Add GOPATH to shell
  (when (memq window-system '(mac ns))
    (exec-path-from-shell-copy-env "GOPATH")
    (exec-path-from-shell-copy-env "PYTHONPATH")
    (exec-path-from-shell-initialize)))

(use-package expand-region
  :bind
  ("C-=" . er/expand-region))

(use-package flycheck)

(use-package paradox
  :config
  (paradox-enable))

(use-package counsel
  :bind
  ("M-x" . counsel-M-x)
  ("C-x C-m" . counsel-M-x)
  ("C-x C-f" . counsel-find-file)
  ("C-c j" . counsel-rg)
  ("C-c g" . counsel-git)
  ("C-x c k" . counsel-yank-pop))

(use-package find-file-in-project
  :config
  (setq ffip-use-rust-fd t))

(use-package counsel-projectile
  :bind
  ("C-x v" . counsel-projectile)
  ("C-x c p" . counsel-projectile-ag)
  :config
  (counsel-projectile-on))

(use-package ivy
  :bind
  ("C-s" . swiper)
  ("C-r" . ivy-resume)
  :config
  (ivy-mode 1)
  (setq ivy-use-virtual-buffers nil
	ivy-use-selectable-prompt t)
  (define-key read-expression-map (kbd "C-r") 'counsel-expression-history))


(use-package hlinum
  :config
  (hlinum-activate))

(use-package linum
  :config
  (setq linum-format " %3d ")
  (global-linum-mode nil))

(use-package magit
  :config

  (setq magit-completing-read-function 'ivy-completing-read)

  :bind
  ;; Magic
  ("C-x g s" . magit-status)
  ("C-x g x" . magit-checkout)
  ("C-x g c" . magit-commit)
  ("C-x g p" . magit-push)
  ("C-x g u" . magit-pull)
  ("C-x g e" . magit-ediff-resolve)
  ("C-x g r" . magit-rebase-interactive))

(use-package magit-popup)

(use-package magit-gitflow
  :after (magit)
  :config
  (add-hook 'magit-mode-hook 'turn-on-magit-gitflow)
  (add-hook 'git-commit-mode-hook 'turn-on-auto-fill))

(use-package multiple-cursors
  :bind
  ("C-S-c C-S-c" . mc/edit-lines)
  ("C->" . mc/mark-next-like-this)
  ("C-<" . mc/mark-previous-like-this)
  ("C-c C->" . mc/mark-all-like-this))

(use-package org
  :config
  (setq org-directory (expand-file-name "~/Documents/Notes")
	org-default-notes-file (expand-file-name
				"~/Documents/Notes/todo-notes.org")
	org-refile-targets '(("~/Documents/Notes/archivo.org" :maxlevel . 1))
	org-src-preserve-indentation t
	org-agenda-files (directory-files "~/Documents/Notes/" t "org$")
	org-todo-keywords  '((sequence "TODO(t)"
				       "STARTED(s@)"
				       "WAITING(w@)"
				       "APPOINTMENT(a@)"
				       "|" "DONE(d!)"
				       "CANCELLED(c@)"
				       "DEFERRED(f@)"))
	org-capture-templates '(("t" "TODO item" entry
				 (file+headline
				  org-default-notes-file "Tareas")
				 "* TODO %? \n %i" :kill-buffer t)
				("b" "Blog item" entry
				 (file+headline
				  "~/Documents/Notes/blog-notes.org"
				  "Articulos")
				 "* TODO %? \n %i" :kill-buffer t)))
  (toggle-input-method)
  :bind
  ("C-c l" . org-store-link)
  ("C-c c" . org-capture)
  ("C-c a" . org-agenda))

(use-package org-projectile
  :config
  (org-projectile-per-project)
  (setq org-projectile-per-project-filepath "todo.org"
	org-agenda-files (append org-agenda-files (org-projectile-todo-files))))

(use-package page-break-lines)

(use-package projectile
  :bind (:map projectile-mode-map
	      ("C-c p" . projectile-command-map))
  :config
  (setq projectile-known-projects-file
        (expand-file-name "projectile-bookmarks.eld" temp-dir))
  (add-to-list 'projectile-project-root-files "package.json")
  (add-to-list 'projectile-project-root-files "__manifest__.py")
  (add-to-list 'projectile-globally-ignored-directories "node_modules")
  (projectile-register-project-type 'npm '("package.json")
                                    :compile "npm install"
                                    :test "npm test"
                                    :run "npm start"
                                    :test-suffix ".spec")
  (setq projectile-indexing-method 'alien)
  (setq projectile-generic-command "fd -H --ignore-file .projectile -t f -0")
  (setq projectile-completion-system 'ivy)
  (projectile-global-mode))

(use-package recentf
  :config
  (setq recentf-save-file (recentf-expand-file-name "~/.emacs.d/private/cache/recentf"))
  (recentf-mode 1))


;;(use-package undo-tree
;;  :config
  ;; Remember undo history
;;  (setq
;;   undo-tree-auto-save-history nil
;;   undo-tree-history-directory-alist `(("." . ,(concat temp-dir "/undo/"))))
;;  (global-undo-tree-mode 1))

(use-package which-key
  :config
  (which-key-mode))

(use-package dired
  :ensure nil
  :config
  (setq ls-lisp-dirs-first t)
  (setq ls-lisp-use-insert-directory-program nil)
  (setq dired-garbage-files-regexp "\\(?:\\.\\(?:aux\\|bak\\|dvi\\|log\\|orig\\|rej\\|toc\\|pyc\\)\\)\\'")
  (setq dired-dwim-target 't))

(use-package dired-single
  :commands (dired)
  :config
  (define-key dired-mode-map [return] 'dired-single-buffer)
  (define-key dired-mode-map [mouse-1] 'dired-single-buffer-mouse)
  (define-key dired-mode-map "^"
    (function
     (lambda nil (interactive) (dired-single-buffer "..")))))

(use-package deadgrep)

(use-package yasnippet
  :config
  (yas-global-mode 1))


(use-package know-your-http-well)

(use-package git)

(use-package editorconfig)

(use-package restclient
  :mode ("\\.http\\'" . restclient-mode)
  :config
  (use-package company-restclient))

(use-package dired-single)

(use-package telephone-line
  :config
   (setq telephone-line-primary-left-separator    'telephone-line-cubed-left
         telephone-line-secondary-left-separator  'telephone-line-cubed-hollow-left
         telephone-line-primary-right-separator   'telephone-line-cubed-right
         telephone-line-secondary-right-separator 'telephone-line-cubed-hollow-right)
  (telephone-line-mode))


(use-package tabbar
  :config
  (tabbar-mode 1))

(provide 'base-extensions)
