(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(js2-strict-inconsistent-return-warning nil)
 '(js2-strict-missing-semi-warning nil)
 '(package-selected-packages
   '(lsp-ui go-dlv company-restclient restclient sly rinari robe enh-ruby-mode racer cargo flycheck-rust tide company-web emmet-mode web-mode-edit-element which-key web-mode use-package undo-tree toml-mode telephone-line tabbar spacemacs-theme rust-mode pyenv-mode py-autopep8 pip-requirements paradox org-projectile markdown-mode magit-gitflow know-your-http-well js2-refactor js-comint impatient-mode hlinum helpful go-eldoc git flycheck expand-region exec-path-from-shell elpy editorconfig dynamic-fonts dired-single deadgrep dashboard counsel-projectile company-tern company-lsp company-go)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
