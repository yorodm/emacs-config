;;; package --- Main init file
;;; Commentary:
;;; This is my init file

;;; Code:
(let ((file-name-handler-alist nil))
  (add-to-list 'load-path (concat user-emacs-directory "elisp"))

  (require 'base)
  (require 'base-theme)
  (require 'base-extensions)
  (require 'base-functions)
  (require 'base-global-keys)

  (require 'lang-python)

  (require 'lang-go)

  (require 'lang-javascript)

  (require 'lang-web)

  (require 'lang-lisp)

  (require 'lang-rust)

  (require 'lang-ruby))
